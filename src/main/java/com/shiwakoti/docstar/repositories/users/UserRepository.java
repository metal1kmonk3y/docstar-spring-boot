package com.shiwakoti.docstar.repositories.users;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.shiwakoti.models.users.User;

public interface UserRepository extends MongoRepository<User, String>{
	public User findByUsername(String username);
}
