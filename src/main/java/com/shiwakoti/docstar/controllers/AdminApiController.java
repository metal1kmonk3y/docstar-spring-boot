package com.shiwakoti.docstar.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.shiwakoti.docstar.services.UserService;
import com.shiwakoti.models.users.User;

@RestController
@RequestMapping("/api/v1/admin")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class AdminApiController {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping( value="/users", method=RequestMethod.GET)
	public List<User> getUsers(){
		return userService.findAll();
	}
	
	@RequestMapping(value ="/users", method=RequestMethod.POST)
	public User createUser(@RequestBody User user){
		return userService.save(user);
	}
	
	@RequestMapping(value="/users/{uid}", method=RequestMethod.DELETE)
	public void deleteUser(@PathVariable String uid){
		userService.delete(userService.findOne(uid));
	}
	
}
