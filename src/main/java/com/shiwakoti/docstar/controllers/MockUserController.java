package com.shiwakoti.docstar.controllers;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.shiwakoti.docstar.repositories.users.UserRepository;
import com.shiwakoti.docstar.services.UserService;
import com.shiwakoti.models.users.Role;
import com.shiwakoti.models.users.User;

@Controller
public class MockUserController {
	@Autowired 
	private UserService userService;
	
	@Autowired
	private UserRepository userRepo;
	
	@PostConstruct
	private void createAnAdmin(){
		if(userRepo.count() > 0){
			System.out.println("admin already exist");
			return;
		}
		
		List<Role> roles = new LinkedList<>();
		List<String> phoneNumbers = new LinkedList<>();	
		roles.add(new Role("ROLE_ADMIN"));
		phoneNumbers.add("608-213-7868");
		phoneNumbers.add("222-213-7868");
		
		User user = new User.Builder()
						.firstName("bilbo").lastName("baggins")
						.roles(roles)
						.username("bilbo")
						.password("123")
						.email("bilbo@shire.com")
						.phoneNumbers(phoneNumbers).build();
		
		userService.save(user);
	}
}
