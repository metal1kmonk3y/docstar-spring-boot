package com.shiwakoti.docstar.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


import com.shiwakoti.docstar.repositories.users.UserRepository;
import com.shiwakoti.models.users.User;

@Service
public class UserService implements UserDetailsService {
	@Autowired
	private UserRepository userRepo;
	
	public User loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepo.findByUsername(username);
		if(user == null) throw new UsernameNotFoundException("username");
		return user;
	} 

	public User save(User user) {
		return userRepo.save(user); 
	}
	
	public List<User> findAll(){
		return userRepo.findAll();
	}
	
	public User findOne(String id) {
		return userRepo.findOne( id );
	}
	
	public void delete(User user) {
		userRepo.delete(user);
	}
}
