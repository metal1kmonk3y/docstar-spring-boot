package com.shiwakoti.models.users;

import java.util.Collection;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@SuppressWarnings("serial")
@Document
public class User implements UserDetails{
	
	@Id
	private String id;
	private String firstName;
	private String lastName;
	private List<Role> roles;
	@Indexed(unique=true)
	private String username;
	private String password;
	private String email;
	private List<String> phoneNumbers;
	
	
	public User(){}	
	
	private User( User.Builder builder) {
		this.id = builder.id;
		this.firstName = builder.firstName;
		this.lastName = builder.lastName;
		this.roles = builder.roles;
		this.username = builder.username;
		this.password = builder.password;
		this.email = builder.email;		
		this.phoneNumbers = builder.phoneNumbers;
	
	}	

	public static class Builder {	
		
		private String id;
		private String firstName;
		private String lastName;
		private List<Role> roles;
		private String username;
		private String password;
		private String email;	
		private List<String> phoneNumbers;
		
		public Builder(){}
		
		public Builder phoneNumbers(List<String> phoneNumbers){
			this.phoneNumbers = phoneNumbers;
			return this;
		}
		
		public Builder password(String password) {
			this.password = password;
			return this;
		}
		
		public Builder roles(List<Role> roles) {
			this.roles = roles;
			return this;
		}
		
		public Builder id(String id) {
			this.id = id;
			return this;
		}
			
		public Builder firstName(String firstName) {
			this.firstName = firstName;
			return this;
		}
		
		public Builder lastName(String lastName) {
			this.lastName = lastName;
			return this;
		}
		
		public Builder username(String username) {
			this.username = username;
			return this;
		}
		
		public Builder email(String email) {
			this.email = email;
			return this;
		}			
		
		public User build() {
			return new User(this);
		}
		
	}	

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {		
		return roles;
	}

	@Override
	public String getPassword() {		
		return password;
	}

	@Override
	public String getUsername() {		
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {		
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {		
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {	
		return true;
	}

	@Override
	public boolean isEnabled() {		
		return true;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<String> getPhoneNumbers() {
		return phoneNumbers;
	}

	public void setPhoneNumbers(List<String> phoneNumebers) {
		this.phoneNumbers = phoneNumebers;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
