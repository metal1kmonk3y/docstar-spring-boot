var profile = angular.module('Docstar.Profile', ['ngRoute', 'ngResource']);

profile.controller('Docstar.Profile.Controller', ['$scope', '$resource', '$location', 
	'$http', 'Authentication', function($scope, $resource, $location,
			$http, Auth){
	
	$scope.user = Auth.user;
	
	$scope.userCopy = $scope.user;
	
}]);