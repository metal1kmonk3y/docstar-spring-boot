var login = angular.module('Docstar.Login', ['ngRoute', 'ngResource']);

login.controller('Docstar.Login.Controller', ['$scope', '$resource', '$location', 
	'$http', 'Authentication', function($scope, $resource, $location,
			$http, Auth){
	
	$scope.credentials = {username: '', password: ''};
	$scope.error = null;
	
	$scope.login = function(){
		console.log("here");
		var copy = $scope.credentials;
		$scope.credentials = {username: '', password: ''};
		$http({
			url: '/login',
			method: 'POST',
			
	         headers : {
	            'Content-Type' : 'application/x-www-form-urlencoded'
	         },
		    data : 'username=' + copy.username + '&password=' + copy.password
		}).then((user) => {
			Auth.setUser(user.data);			
			$location.path('/docstar');			
		}, (response) => {
			$scope.error = response.data;					
		});
		
	}
	
}]);