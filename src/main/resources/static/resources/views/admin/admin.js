var admin = angular.module('Docstar.Admin', ['ngRoute', 'ngResource']);

admin.controller('Docstar.Admin.Controller', ['$scope', '$resource', '$location', 
	'$http', 'Authentication', function($scope, $resource, $location,
			$http, Auth){
	
	$scope.isCreatingTime = false;
	
	var Users = $resource('api/v1/admin/users/:uid', {});
	
	$scope.newUser = {
		firstName: '',
		lastName : '',
		roles : [],			
		username :  '',
		password : '',
		email : '',
		phoneNumbers : []
	}
	
	$scope.role = 'ROLE_USER';
	
	$scope.phoneNum = '111-222-3333 ; 444-555-6666';
	
	$scope.users = Users.query();
	
	$scope.createNewUser = _ => {$scope.isCreatingTime = true;}
	
	$scope.createUser = _ =>{	
		$scope.newUser.roles.push($scope.role);
		$scope.role = 'ROLE_USER';
		
		$scope.phoneNum.split(';').forEach(phn => {$scope.newUser.phoneNumbers.push(phn); })		
		$scope.phoneNum = '111-222-3333 ; 444-555-6666';			
		
		Users.save( {},
			  	 $scope.newUser,
	            function( response ) {
	                 $scope.users.push( response );
	                 $scope.isCreatingTime = false;
	 			 });
	}
	
	$scope.cancelCreateUser = _ => {$scope.isCreatingTime = false;}
	
	$scope.deleteUser = id => {
		
		Users.delete({uid: id }, _ => {
			window.location.reload();
		})
	}


	
}]);