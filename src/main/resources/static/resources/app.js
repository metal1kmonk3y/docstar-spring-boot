var app = angular.module('Docstar.App', [
    'ngRoute',
    'spring-security-csrf-token-interceptor',
    'Docstar.Login',
    'Docstar.Profile',
    'Docstar.Admin',
    'Docstar.User'
]);

app.service('Authentication', function(){
	var user = sessionStorage.user ? JSON.parse( sessionStorage.user ) : null;
	
	this.user = user;
	this.setUser = u => {sessionStorage.user = JSON.stringify(u); user = u; window.location.reload();},
	this.isAuthenticated = _ => { return user != null && user.id != null},
	this.clearUser = _ => {delete sessionStorage.user, user = null; window.location.reload(); }	
		
});

app.controller('Docstar.Controller',[ '$scope', 'Authentication', '$location', '$http', function($scope, Auth , $location, $http){	
	
	$scope.isAuthenticated = Auth.isAuthenticated;
	
	$scope.hasRole = role => {	
		var user = Auth.user;
		
		if( user) {		   
		   for( var i = 0; i < user.roles.length; i++) {
			   if( user.roles[i].name === role ) return true;
		   }
		}
		return false;	
	}
		
	$scope.logout = function() {
		var req = {
				method : 'GET',
				url : '/logout',
					};
			
		$http(req).then(function() {
			Auth.clearUser();			
			$location.path('/login');
			
		});
	}
}]);

app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider){
    $locationProvider.html5Mode(false);
   
    $routeProvider
    .when('/docstar', {
        templateUrl: '/resources/views/docstar.html',
    	controller: 'Docstar.Controller',
        resolve:{
            "check":function($location, Authentication){   
                if(!Authentication.isAuthenticated()){                 	
                    $location.path('/login');
                }
            }
        }       
    })
    .when('/login',{
    	templateUrl: '/resources/views/login/login.html',   
    	controller: 'Docstar.Login.Controller',
        resolve:{
            "check":function($location, Authentication){   
                if(Authentication.isAuthenticated()){                 	
                    $location.path('/docstar');
                }
            }
        }  		
	})
	.when('/profile',{
		templateUrl: '/resources/views/shared/profile.html',
    	controller: 'Docstar.Profile.Controller',
        resolve:{
            "check":function($location, Authentication){   
                if(!Authentication.isAuthenticated()){                 	
                    $location.path('/login');
                }
            }
        }
	})
	.when('/admin',{
		templateUrl: '/resources/views/admin/admin.html',
    	controller: 'Docstar.Admin.Controller',
        resolve:{
            "check":function($location, Authentication){   
                if(!Authentication.isAuthenticated()){                 	
                    $location.path('/login');
                }
            }
        }
	})
	.when('/user',{
		templateUrl: '/resources/views/user/user.html',
    	controller: 'Docstar.User.Controller',
        resolve:{
            "check":function($location, Authentication){   
                if(!Authentication.isAuthenticated()){                 	
                    $location.path('/login');
                }
            }
        }
	})
	.otherwise({
        redirectTo:'/login'
    })
}]);